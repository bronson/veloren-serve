# veloren-serve

A dockerfile and some scripts to make it a little easier to host Veloren servers.

Game state is stored as Sqlite dump files, so Git does a much _much_ better job
of storing it.

TODO: link to blog post

## Usage

1. Fork this repo into your account.
1. Clone that repo onto the machine you'll use as the Veloren server
  * git clone https://gitlab.com/YOUR-ACCOUNT/veloren-serve.git
1. `cd veloren-serve`

To preserve an existing game...

1. If you already have a userdata directory you want to use (for example, you were playing singleplayer and want to continue with that world), then follow these directions. If not, just skip ahead to "Launch the server".
  * Replace the existing userdata directory with your game:
    * `git rm -r userdata`
    * `cp -r ~/my/userdata .`
    * `git add userdata`
  * Turn it into a dumpfile, ignore any errors about docker-compose failing.
    * `./suspend`

Launch the server

1. Run the `./resume` script. When the logs say that the server is accepting connections, start playing!
  * `./resume`
1. When you're done, run suspend to stop the server, store the gamestate, and push it to your repo.
  * `./suspend`
1. If your changes were pushed without error, you're done. You can even delete the local copy of this repo. Everything is stored upstream.

When you want to play again, run `./resume`, wait for it to accept connections, and start playing.

When you're done, `./suspend`.

Repeat until your life becomes sunflower iced tea.

NOTE: these scripts are typically run as root on disposable hosts.
If you run into errors, and you're running as a regular user,
there's a good chance that boosting your privlileges will help: `sudo ./resume`.

## Updating to a new veloren-serve

If changes appear in this repo that you would like to bring into your game repo, do this:

* `git remote add upstream https://gitlab.com/bronson/veloren-serve.git`
* `git pull upstream main`

This will merge upstream changes into your repo.

You only need to add the upstream once. If the upstream is already added,
just run the git pull.

## License

MIT, use freely.
